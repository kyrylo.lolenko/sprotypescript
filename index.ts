import Student from "./classes/Student";
import Teacher from "./classes/Teacher";
import Course from "./classes/Course";
import { University } from "./classes/University";
import { CourseStatus } from "./enums/CourseStatus";

const university = new University();

const Kyrylo = new Student(1, "Kyrylo", 20);
const Liza = new Student(2, "Liza", 20);

const Huzhva = new Teacher(1, "Huzhva", 40);
const Hordienko = new Teacher(2, "Hordienko", 60);

const mathCourse = new Course(1, "Maths", Huzhva, CourseStatus.Open);
const englishCourse = new Course(2, "English", Hordienko, CourseStatus.Closed);

Kyrylo.enroll(mathCourse);
Liza.enroll(englishCourse);

mathCourse.addStudent(Kyrylo);

englishCourse.addStudent(Kyrylo);

englishCourse.addStudent(Liza);

university.addMember(Huzhva);

university.addMember(Liza);

university.addCourse(mathCourse);
university.addCourse(englishCourse);
university.changeCourseStatus(mathCourse, CourseStatus.Closed);

console.log(Liza.greet());
console.log(Kyrylo.enrolledCourses);
console.log(Hordienko.coursesTaught);
