export default function BaseEntity<T extends new (...args: any[]) => {}>(
  ctr: T,
  context?: any
): T {
  return class extends ctr {
    id = Math.random();
    created = new Date().toLocaleString("en-US");
  } as T;
}
