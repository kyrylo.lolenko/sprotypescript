export default function ValidateName(target: any, key: string) {
  let value = target[key];
  const getter = () => value;
  const setter = (newVal: string) => {
    if (newVal.length < 3) {
      throw new Error("Course name must be at least 3 characters long.");
    }
    value = newVal;
  };
  Object.defineProperty(target, key, {
    get: getter,
    set: setter,
    enumerable: true,
    configurable: true,
  });
}
