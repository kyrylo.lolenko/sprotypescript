export default function CourseNameDecorator(
  target: any,
  key: string,
  descriptor: PropertyDescriptor
): PropertyDescriptor {
  const originalGet = descriptor.get;
  const originalSet = descriptor.set;

  descriptor.get = function () {
    if (originalGet) {
      console.log(`Accessing course name: ${originalGet.call(this)}`);
      return originalGet.call(this);
    }
  };

  descriptor.set = function (newName: string) {
    if (originalSet) {
      console.log(
        `Modifying course name from ${originalGet?.call(this)} to ${newName}`
      );
      originalSet.call(this, newName);
    }
  };

  return descriptor;
}
