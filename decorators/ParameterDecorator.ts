import IStudent from "../interfaces/IStudent";

export default function ValidateStudent(
  target: any,
  propertyKey: string,
  parameterIndex: number
) {
  const originalMethod = target[propertyKey];
  target[propertyKey] = function (...args: any[]) {
    const student: IStudent = args[parameterIndex];
    if (!student || !student.name || !student.id) {
      throw new Error("Invalid student object.");
    }
    return originalMethod.apply(this, args);
  };
}
