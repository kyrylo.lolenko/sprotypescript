export enum CourseStatus {
  Open = "Open",
  Closed = "Closed",
  Completed = "Completed",
}
