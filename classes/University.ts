import ITeacher from "../interfaces/ITeacher";
import IStudent from "../interfaces/IStudent";
import ICourse from "../interfaces/ICourse";
import { UniversityMember } from "../types/UniversityMember";
import { CourseStatus } from "../enums/CourseStatus";
import { isStudent, isTeacher } from "../types/UniversityMember";

export class University {
  private members: UniversityMember[] = [];
  private courses: ICourse[] = [];

  addMember(member: UniversityMember): void {
    this.members.push(member);
  }

  addCourse(course: ICourse): void {
    this.courses.push(course);
  }

  changeCourseStatus(course: ICourse, status: CourseStatus): void {
    course.status = status;
  }

  findStudents(): IStudent[] {
    return this.members.filter(isStudent);
  }

  findTeachers(): ITeacher[] {
    return this.members.filter(isTeacher);
  }

  createCourseWithoutTeacher(
    course: Omit<ICourse, "teacher" | "status">
  ): ICourse {
    const newCourse = { ...course, teacher: null, status: CourseStatus.Open };
    this.courses.push(newCourse);
    return newCourse;
  }
}
