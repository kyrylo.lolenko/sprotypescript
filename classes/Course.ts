import BaseEntity from "../decorators/ClassDecorator";
import ValidateName from "../decorators/PropertyDecorator";
import ValidateStudent from "../decorators/ParameterDecorator";
import LogExecutionTime from "../decorators/MethodDecorator";
import CourseNameDecorator from "../decorators/AccessorDecorator";
import { CourseStatus } from "../enums/CourseStatus";
import ICourse from "../interfaces/ICourse";
import IStudent from "../interfaces/IStudent";
import ITeacher from "../interfaces/ITeacher";

@BaseEntity
export default class Course implements ICourse {
  private _students: IStudent[] = [];

  private _name: string = "";

  constructor(
    public id: number,
    name: string,
    public teacher: ITeacher,
    public status: CourseStatus = CourseStatus.Open
  ) {
    this._name = name;
  }

  @ValidateName
  @CourseNameDecorator
  public get name(): string {
    return this._name;
  }

  public set name(value: string) {
    this._name = value;
  }

  get students(): IStudent[] {
    return this._students;
  }

  @LogExecutionTime
  addStudent(@ValidateStudent student: IStudent): void {
    this._students.push(student);
  }
}
