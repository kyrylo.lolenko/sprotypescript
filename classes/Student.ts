import AbstractPerson from "./AbstractPerson";
import { Role } from "../enums/Role";
import ICourse from "../interfaces/ICourse";

export default class Student extends AbstractPerson {
  constructor(
    id: number,
    name: string,
    age: number,
    public enrolledCourses: ICourse[] = []
  ) {
    super(id, name, age);
  }

  enroll(course: ICourse): void {
    this.enrolledCourses.push(course);
  }

  getRole(): string {
    return Role.Student;
  }
}
