import AbstractPerson from "./AbstractPerson";
import { Role } from "../enums/Role";
import ICourse from "../interfaces/ICourse";

export default class Teacher extends AbstractPerson {
  constructor(
    id: number,
    name: string,
    age: number,
    public coursesTaught: ICourse[] = []
  ) {
    super(id, name, age);
  }

  teach(course: ICourse): void {
    this.coursesTaught.push(course);
  }

  getRole(): string {
    return Role.Teacher;
  }
}
