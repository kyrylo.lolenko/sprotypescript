export default abstract class AbstractPerson {
  constructor(public id: number, public name: string, public age: number) {}

  abstract getRole(): string;

  greet(): string {
    return `Hello, my name is ${this.name} and I'm ${this.age} years old.`;
  }
}
