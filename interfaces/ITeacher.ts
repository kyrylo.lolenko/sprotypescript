import ICourse from "./ICourse";

export default interface ITeacher {
  id: number;
  name: string;
  age: number;
  coursesTaught: ICourse[];
  teach(course: ICourse): void;
}
