import ICourse from "./ICourse";

export default interface IStudent {
  id: number;
  name: string;
  age: number;
  enrolledCourses: ICourse[];
  enroll(course: ICourse): void;
}
