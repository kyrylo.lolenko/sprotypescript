import ITeacher from "./ITeacher";
import { CourseStatus } from "../enums/CourseStatus";

export default interface ICourse {
  id: number;
  name: string;
  teacher: ITeacher | null;
  status: CourseStatus;
}
