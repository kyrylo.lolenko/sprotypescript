import IStudent from "../interfaces/IStudent";
import ITeacher from "../interfaces/ITeacher";

export type UniversityMember = IStudent | ITeacher;

export function isStudent(member: UniversityMember): member is IStudent {
  return (member as IStudent).enrolledCourses !== undefined;
}

export function isTeacher(member: UniversityMember): member is ITeacher {
  return (member as ITeacher).coursesTaught !== undefined;
}
